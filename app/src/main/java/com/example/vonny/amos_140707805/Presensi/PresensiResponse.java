package com.example.vonny.amos_140707805.Presensi;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PresensiResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private List<PresensiModel> data;

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(List<PresensiModel> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<PresensiModel> getData() {
        return data;
    }
}
