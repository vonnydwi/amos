package com.example.vonny.amos_140707805.Presensi;

import com.google.gson.annotations.SerializedName;

public class PresensiModel {


    @SerializedName("h1")
    private String h1;
    @SerializedName("h2")
    private String h2;
    @SerializedName("h3")
    private String h3;
    @SerializedName("h4")
    private String h4;
    @SerializedName("h5")
    private String h5;
    @SerializedName("h6")
    private String h6;
    @SerializedName("h7")
    private String h7;
    @SerializedName("h8")
    private String h8;
    @SerializedName("h9")
    private String h9;
    @SerializedName("h10")
    private String h10;
    @SerializedName("h11")
    private String h11;
    @SerializedName("h12")
    private String h12;
    @SerializedName("h13")
    private String h13;
    @SerializedName("h14")
    private String h14;
    @SerializedName("h15")
    private String h15;
    @SerializedName("h16")
    private String h16;
    @SerializedName("h17")
    private String h17;
    @SerializedName("h18")
    private String h18;
    @SerializedName("h19")
    private String h19;
    @SerializedName("h20")
    private String h20;
    @SerializedName("h21")
    private String h21;
    @SerializedName("h22")
    private String h22;
    @SerializedName("h23")
    private String h23;
    @SerializedName("h24")
    private String h24;
    @SerializedName("h25")
    private String h25;
    @SerializedName("h26")
    private String h26;
    @SerializedName("h27")
    private String h27;
    @SerializedName("h28")
    private String h28;
    @SerializedName("h29")
    private String h29;
    @SerializedName("h30")
    private String h30;
    @SerializedName("h31")
    private String h31;

    public void setH1(String h1) {
        this.h1 = h1;
    }

    public void setH2(String h2) {
        this.h2 = h2;
    }

    public void setH3(String h3) {
        this.h3 = h3;
    }

    public void setH4(String h4) {
        this.h4 = h4;
    }

    public void setH5(String h5) {
        this.h5 = h5;
    }

    public void setH6(String h6) {
        this.h6 = h6;
    }

    public void setH7(String h7) {
        this.h7 = h7;
    }

    public void setH8(String h8) {
        this.h8 = h8;
    }

    public void setH9(String h9) {
        this.h9 = h9;
    }

    public void setH10(String h10) {
        this.h10 = h10;
    }

    public void setH11(String h11) {
        this.h11 = h11;
    }

    public void setH12(String h12) {
        this.h12 = h12;
    }

    public void setH13(String h13) {
        this.h13 = h13;
    }

    public void setH14(String h14) {
        this.h14 = h14;
    }

    public void setH15(String h15) {
        this.h15 = h15;
    }

    public void setH16(String h16) {
        this.h16 = h16;
    }

    public void setH17(String h17) {
        this.h17 = h17;
    }

    public void setH18(String h18) {
        this.h18 = h18;
    }

    public void setH19(String h19) {
        this.h19 = h19;
    }

    public void setH20(String h20) {
        this.h20 = h20;
    }

    public void setH21(String h21) {
        this.h21 = h21;
    }

    public void setH22(String h22) {
        this.h22 = h22;
    }

    public void setH23(String h23) {
        this.h23 = h23;
    }

    public void setH24(String h24) {
        this.h24 = h24;
    }

    public void setH25(String h25) {
        this.h25 = h25;
    }

    public void setH26(String h26) {
        this.h26 = h26;
    }

    public void setH27(String h27) {
        this.h27 = h27;
    }

    public void setH28(String h28) {
        this.h28 = h28;
    }

    public void setH29(String h29) {
        this.h29 = h29;
    }

    public void setH30(String h30) {
        this.h30 = h30;
    }

    public void setH31(String h31) {
        this.h31 = h31;
    }


    public String getH1() {
        return h1;
    }

    public String getH2() {
        return h2;
    }

    public String getH3() {
        return h3;
    }

    public String getH4() {
        return h4;
    }

    public String getH5() {
        return h5;
    }

    public String getH6() {
        return h6;
    }

    public String getH7() {
        return h7;
    }

    public String getH8() {
        return h8;
    }

    public String getH9() {
        return h9;
    }

    public String getH10() {
        return h10;
    }

    public String getH11() {
        return h11;
    }

    public String getH12() {
        return h12;
    }

    public String getH13() {
        return h13;
    }

    public String getH14() {
        return h14;
    }

    public String getH15() {
        return h15;
    }

    public String getH16() {
        return h16;
    }

    public String getH17() {
        return h17;
    }

    public String getH18() {
        return h18;
    }

    public String getH19() {
        return h19;
    }

    public String getH20() {
        return h20;
    }

    public String getH21() {
        return h21;
    }

    public String getH22() {
        return h22;
    }

    public String getH23() {
        return h23;
    }

    public String getH24() {
        return h24;
    }

    public String getH25() {
        return h25;
    }

    public String getH26() {
        return h26;
    }

    public String getH27() {
        return h27;
    }

    public String getH28() {
        return h28;
    }

    public String getH29() {
        return h29;
    }

    public String getH30() {
        return h30;
    }

    public String getH31() {
        return h31;
    }

}


