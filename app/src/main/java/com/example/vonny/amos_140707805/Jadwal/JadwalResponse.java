package com.example.vonny.amos_140707805.Jadwal;

import com.example.vonny.amos_140707805.Jadwal.JadwalModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JadwalResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private List<JadwalModel> data;

    public void setData(List<JadwalModel> data) {
        this.data = data;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<JadwalModel> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }
}
