package com.example.vonny.amos_140707805.Nilai;

import com.google.gson.annotations.SerializedName;

public class NilaiRequest {

    @SerializedName("id_siswa")
    private String idsiswa;

    @SerializedName("nama_mapel")
    private String idmapel;

    @SerializedName("id_kelas")
    private String idkelas;

    public void setIdmapel(String idmapel) {
        this.idmapel = idmapel;
    }

    public void setIdkelas(String idkelas) {
        this.idkelas = idkelas;
    }

    public void setIdsiswa(String idsiswa) {
        this.idsiswa = idsiswa;
    }

    public String getIdmapel() {
        return idmapel;
    }

    public String getIdkelas() {
        return idkelas;
    }

    public String getIdsiswa() {
        return idsiswa;
    }
}
