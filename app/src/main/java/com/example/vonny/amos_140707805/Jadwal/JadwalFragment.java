package com.example.vonny.amos_140707805.Jadwal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.vonny.amos_140707805.EndPoint;
import com.example.vonny.amos_140707805.R;
import com.example.vonny.amos_140707805.RetrofitInstance;
import com.example.vonny.amos_140707805.SessionManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JadwalFragment extends Fragment {

    String hari = "senin";
    Spinner spinnername;
    SessionManager sessionManager;
    public List<JadwalModel> jadwalModelList = new ArrayList<>();
    private JadwalAdapter jadwalAdapter;
    RecyclerView recyclerView;
    ProgressDialog progressDialog;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_jadwal, container, false);
    }

    private void getDataJadwal(JadwalRequest jadwalRequest)
    {

        EndPoint service = RetrofitInstance.getRetrofitInstance(getContext()).create(EndPoint.class);
        Call<JadwalResponse> call = service.doJadwal(jadwalRequest);
        call.enqueue(new Callback<JadwalResponse>() {
            @Override
            public void onResponse(Call<JadwalResponse> call, Response<JadwalResponse> response) {
                getData(response.body().getData());
                Log.d("oit",response.body().getData().toString());

            }

            @Override
            public void onFailure(Call<JadwalResponse> call, Throwable t) {

            }
        });
    }


    private void getData(List<JadwalModel> data) {
        jadwalModelList.clear();
        jadwalModelList.addAll(data);
        jadwalAdapter.notifyDataSetChanged();
        progressDialog.hide();
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.rv_jadwal);
        jadwalAdapter = new JadwalAdapter(getContext(),jadwalModelList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(jadwalAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),LinearLayoutManager.HORIZONTAL));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),LinearLayoutManager.VERTICAL));

        spinnername = view.findViewById(R.id.spinerjadwal);
        spinnername.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                Log.d("oyyyyy", "test :" + String.valueOf(position));
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("loading");
                progressDialog.show();
                progressDialog.setCancelable(false);
                if(position==0)
                {
                    sessionManager = new SessionManager(getContext());
                    hari="senin";
                    JadwalRequest request = new JadwalRequest();
                    request.setHari(hari);
                    request.setKelas(sessionManager.getKeyIdKelassiswa());
                    getDataJadwal(request);

                }
                else if(position==1)
                {
                    sessionManager = new SessionManager(getContext());
                    hari="selasa";
                    JadwalRequest request = new JadwalRequest();
                    request.setHari(hari);
                    request.setKelas(sessionManager.getKeyIdKelassiswa());
                    getDataJadwal(request);
                }
                else if(position==2)
                {
                    sessionManager = new SessionManager(getContext());
                    hari="rabu";
                    JadwalRequest request = new JadwalRequest();
                    request.setHari(hari);
                    request.setKelas(sessionManager.getKeyIdKelassiswa());
                    getDataJadwal(request);
                }
                else if(position==3)
                {
                    sessionManager = new SessionManager(getContext());
                    hari="kamis";
                    JadwalRequest request = new JadwalRequest();
                    request.setHari(hari);
                    request.setKelas(sessionManager.getKeyIdKelassiswa());
                    getDataJadwal(request);
                }
                else if(position==4)
                {
                    sessionManager = new SessionManager(getContext());
                    hari="jumat";
                    JadwalRequest request = new JadwalRequest();
                    request.setHari(hari);
                    request.setKelas(sessionManager.getKeyIdKelassiswa());
                    getDataJadwal(request);
                }
                else if(position==5)
                {
                    sessionManager = new SessionManager(getContext());
                    hari="sabtu";
                    JadwalRequest request = new JadwalRequest();
                    request.setHari(hari);
                    request.setKelas(sessionManager.getKeyIdKelassiswa());
                    getDataJadwal(request);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });



  }




}
