package com.example.vonny.amos_140707805;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


public class ProfilFragment extends Fragment {

    SessionManager sessionManager;
    TextView nama, kelas, kelamin, nisn, nis, tmptlahir, tgllahir, agama, alamat, ortu, abc;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profil, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SessionManager sessionManager = new SessionManager(getContext());
        nama = view.findViewById(R.id.id_nama);
        Log.d("oyyyyy","test:"+sessionManager.getKeyNamasiswa());
        nama.setText(sessionManager.getKeyNamasiswa());

        abc = view.findViewById(R.id.abcd);
        sessionManager = new SessionManager(getContext());
        String inisial = sessionManager.getKeyNamasiswa().substring(0,1).toUpperCase();
        abc.setText(inisial);

        kelamin = view.findViewById(R.id.id_kelamin);
        switch (sessionManager.getKeyKelaminsiswa()) {
            case "L":
                kelamin.setText("Laki Laki");
                break;
            default:
                kelamin.setText("Perempuan");
                break;
        }

        nisn = view.findViewById(R.id.id_nisn);
        nisn.setText(sessionManager.getKeyNisnsiswa());

        nis = view.findViewById(R.id.id_nis);
        nis.setText(sessionManager.getKeyNissiswa());

        tmptlahir = view.findViewById(R.id.id_tmptlahir);
        tmptlahir.setText(sessionManager.getKeyTmptlahirsiswa());

        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
// pass your DOB String
        DateTime jodatime = dtf.parseDateTime(sessionManager.getKeyTgllahirsiswa());
// Format for output
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd-MMMM-yyyy");
// Print the date
        System.out.println(dtfOut.print(jodatime));

        tgllahir = view.findViewById(R.id.id_tgllahir);
        tgllahir.setText(dtfOut.print(jodatime));

        agama = view.findViewById(R.id.id_agama);
        agama.setText(sessionManager.getKeyAgamasiswa());

        alamat = view.findViewById(R.id.id_alamat);
        alamat.setText(sessionManager.getKeyAlamatsiswa());

        ortu = view.findViewById(R.id.id_ayah);
        ortu.setText(sessionManager.getKeyOrtusiswa());

        kelas = view.findViewById(R.id.id_kelas);
        switch (sessionManager.getKeyIdKelassiswa()) {
            case "1":
                kelas.setText("X-AK (Akuntansi)");
                break;
            case "2":
                kelas.setText("X-TKJ Teknik Komputer");
                break;
            default:
                kelas.setText("X-TM Teknik Mesin");
                break;
        }
    }
}
