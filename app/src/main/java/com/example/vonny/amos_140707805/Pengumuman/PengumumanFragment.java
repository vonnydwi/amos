package com.example.vonny.amos_140707805.Pengumuman;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vonny.amos_140707805.EndPoint;
import com.example.vonny.amos_140707805.R;
import com.example.vonny.amos_140707805.RetrofitInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PengumumanFragment extends Fragment {

    private List<PengumumanModel> pengumumanModelList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PengumumanAdapter pengumumanAdapter;
    ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pengumuman, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.rv_main);
        pengumumanAdapter = new PengumumanAdapter(getContext(),pengumumanModelList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(pengumumanAdapter);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("loading");
        progressDialog.show();
        progressDialog.setCancelable(false);
        EndPoint service = RetrofitInstance.getRetrofitInstance(getContext()).create(EndPoint.class);
        Call<PengumumanResponse> call = service.getPengumuman();
        call.enqueue(new Callback<PengumumanResponse>() {
            @Override
            public void onResponse(Call<PengumumanResponse> call, Response<PengumumanResponse> response) {
                getData(response.body().getData());
                progressDialog.hide();
            }

            @Override
            public void onFailure(Call<PengumumanResponse> call, Throwable t) {

            }
        });

    }
    private void getData(List<PengumumanModel> data) {
        pengumumanModelList.addAll(data);
        pengumumanAdapter.notifyDataSetChanged();
    }
}
