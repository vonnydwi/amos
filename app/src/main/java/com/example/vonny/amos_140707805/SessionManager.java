package com.example.vonny.amos_140707805;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SessionManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "AndroidHivePref";
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_TELOR = "telur";
    public static final String KEY_TANGGAL = "tanggal";

    public static final String KEY_IDSISWA = "id_siswa";
    public static final String KEY_NAMASISWA = "nama";
    public static final String KEY_NISNSISWA = "nisn";
    public static final String KEY_NISSISWA = "nis";
    public static final String KEY_ID_KELASSISWA = "id_kelas";
    public static final String KEY_TMPTLAHIRSISWA = "tempat_lahir";
    public static final String KEY_TGLLAHIRSISWA = "tgl_lahir";
    public static final String KEY_AGAMASISWA = "agama";
    public static final String KEY_ALAMATSISWA = "tempat_tinggal";
    public static final String KEY_ORTUSISWA = "nama_ayah";
    public static final String KEY_KELAMINSISWA = "gender";
    public static final String KEY_PASSSISWA = "password";
    public static final String KEY_PHOTO_SISWA = "foto";
    public static final String KEY_FLGALUMNISISWA = "flg_alumni";


    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String email, String idsiswa, String namasiswa, String nisnsiswa, String nissiswa, String idkelassiswa, String tempatlahirsiswa, String tgllahirsiswa, String agamasiswa, String alamatsiswa, String ortusiswa, String kelaminsiswa, String passiswa, String potosiswa, String alumnisiswa){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        editor.putString(KEY_IDSISWA, idsiswa);
        editor.putString(KEY_NAMASISWA,namasiswa);
        editor.putString(KEY_NISNSISWA,nisnsiswa);
        editor.putString(KEY_NISSISWA,nissiswa);
        editor.putString(KEY_ID_KELASSISWA,idkelassiswa);
        editor.putString(KEY_TMPTLAHIRSISWA,tempatlahirsiswa);
        editor.putString(KEY_TGLLAHIRSISWA,tgllahirsiswa);
        editor.putString(KEY_AGAMASISWA,agamasiswa);
        editor.putString(KEY_ALAMATSISWA,alamatsiswa);
        editor.putString(KEY_ORTUSISWA,ortusiswa);
        editor.putString(KEY_KELAMINSISWA,kelaminsiswa);
        editor.putString(KEY_PASSSISWA,passiswa);
        editor.putString(KEY_PHOTO_SISWA,potosiswa);
        editor.putString(KEY_FLGALUMNISISWA,alumnisiswa);


        // Storing email in pref
        editor.putString(KEY_EMAIL, email);

        // commit changes
        editor.commit();
    }


    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_TOKEN, pref.getString(KEY_TOKEN, null));

        // user email id
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

        // return user
        return user;
    }


    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
    public String getToken(){
        return pref.getString(KEY_TOKEN,"");
    }
    public  void setSimpanTelur (String telur, String tanggal){
        editor.putString(KEY_TELOR,telur);
        editor.putString(KEY_TANGGAL,tanggal);
        editor.commit();
    }

    public  void setProfil (String idsiswa, String namasiswa, String nisnsiswa, String nissiswa, String idkelassiswa, String tempatlahirsiswa, String tgllahirsiswa, String agamasiswa, String alamatsiswa, String ortusiswa, String kelaminsiswa, String passiswa, String potosiswa, String alumnisiswa){
        editor.putString(KEY_IDSISWA,idsiswa);
        editor.putString(KEY_NAMASISWA,namasiswa);
        editor.putString(KEY_NISNSISWA,nisnsiswa);
        editor.putString(KEY_NISSISWA,nissiswa);
        editor.putString(KEY_ID_KELASSISWA,idkelassiswa);
        editor.putString(KEY_TMPTLAHIRSISWA,tempatlahirsiswa);
        editor.putString(KEY_TGLLAHIRSISWA,tgllahirsiswa);
        editor.putString(KEY_AGAMASISWA,agamasiswa);
        editor.putString(KEY_ALAMATSISWA,alamatsiswa);
        editor.putString(KEY_ORTUSISWA,ortusiswa);
        editor.putString(KEY_KELAMINSISWA,kelaminsiswa);
        editor.putString(KEY_PASSSISWA,passiswa);
        editor.putString(KEY_PHOTO_SISWA,potosiswa);
        editor.putString(KEY_FLGALUMNISISWA,alumnisiswa);
        editor.commit();
    }

    public String getKeyIdsiswa() {return pref.getString(KEY_IDSISWA, "");}
    public String getKeyNamasiswa() {return pref.getString(KEY_NAMASISWA, "");}
    public String getKeyNisnsiswa() {return pref.getString(KEY_NISNSISWA, "");}
    public String getKeyNissiswa() {return pref.getString(KEY_NISSISWA, "");}
    public String getKeyIdKelassiswa() {return pref.getString(KEY_ID_KELASSISWA, "");}
    public String getKeyTmptlahirsiswa() {return pref.getString(KEY_TMPTLAHIRSISWA, "");}
    public String getKeyTgllahirsiswa() {return pref.getString(KEY_TGLLAHIRSISWA, "");}
    public String getKeyAgamasiswa() {return pref.getString(KEY_AGAMASISWA, "");}
    public String getKeyAlamatsiswa() {return pref.getString(KEY_ALAMATSISWA, "");}
    public String getKeyOrtusiswa() {return pref.getString(KEY_ORTUSISWA, "");}
    public String getKeyKelaminsiswa() {return pref.getString(KEY_KELAMINSISWA, "");}
    public String getKeyPasssiswa() {return pref.getString(KEY_PASSSISWA, "");}
    public String getKeyPhotoSiswa() {return pref.getString(KEY_PHOTO_SISWA, "");}
    public String getKeyFlgalumnisiswa() {return pref.getString(KEY_FLGALUMNISISWA, "");}

}
