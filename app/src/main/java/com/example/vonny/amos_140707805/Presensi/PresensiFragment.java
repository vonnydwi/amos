package com.example.vonny.amos_140707805.Presensi;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.example.vonny.amos_140707805.EndPoint;
import com.example.vonny.amos_140707805.R;
import com.example.vonny.amos_140707805.RetrofitInstance;
import com.example.vonny.amos_140707805.SessionManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresensiFragment extends Fragment {

    private List<PresensiModel> presensiModelList = new ArrayList<>();
    RecyclerView recyclerView;
    PresensiAdapter presensiAdapter;
    PresensiRequest presensiRequest;
    SessionManager sessionManager;
    String idsiswa;
    ProgressDialog progressDialog;
    Spinner tahun, bulan;
    String namabulan = "Januari";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_presensi, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.rv_main);
        presensiAdapter = new PresensiAdapter(getContext(),presensiModelList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(presensiAdapter);

        //recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),LinearLayoutManager.HORIZONTAL));
       // recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),LinearLayoutManager.VERTICAL));


        bulan = view.findViewById(R.id.spinerbulan);

        bulan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(position == 0)
                {
                    sessionManager = new SessionManager(getContext());
                    namabulan = "Januari";
                    PresensiRequest presensiRequest = new PresensiRequest();
                    presensiRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                    presensiRequest.setBulan(String.valueOf(position+1));
                    reqPresensi(presensiRequest);
                }
                else if(position==1)
                {
                    sessionManager = new SessionManager(getContext());
                    namabulan = "Februari";
                    PresensiRequest presensiRequest = new PresensiRequest();
                    presensiRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                    presensiRequest.setBulan(String.valueOf(position+1));
                    reqPresensi(presensiRequest);
                }
                else if(position==2)
                {
                    sessionManager = new SessionManager(getContext());
                    namabulan = "Maret";
                    PresensiRequest presensiRequest = new PresensiRequest();
                    presensiRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                    presensiRequest.setBulan(String.valueOf(position+1));
                    reqPresensi(presensiRequest);
                }
                else if(position==3)
                {
                    sessionManager = new SessionManager(getContext());
                    namabulan = "April";
                    PresensiRequest presensiRequest = new PresensiRequest();
                    presensiRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                    presensiRequest.setBulan(String.valueOf(position+1));
                    reqPresensi(presensiRequest);
                }
                else if(position==4)
                {
                    sessionManager = new SessionManager(getContext());
                    namabulan = "Mei";
                    PresensiRequest presensiRequest = new PresensiRequest();
                    presensiRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                    presensiRequest.setBulan(String.valueOf(position+1));
                    reqPresensi(presensiRequest);
                }
                else if(position==5)
                {
                    sessionManager = new SessionManager(getContext());
                    namabulan = "Juni";
                    PresensiRequest presensiRequest = new PresensiRequest();
                    presensiRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                    presensiRequest.setBulan(String.valueOf(position+1));
                    reqPresensi(presensiRequest);
                }
                else if(position==6)
                {
                    sessionManager = new SessionManager(getContext());
                    namabulan = "Juli";
                    PresensiRequest presensiRequest = new PresensiRequest();
                    presensiRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                    presensiRequest.setBulan(String.valueOf(position+1));
                    reqPresensi(presensiRequest);
                }
                else if(position==7)
                {
                    sessionManager = new SessionManager(getContext());
                    namabulan = "Agustus";
                    PresensiRequest presensiRequest = new PresensiRequest();
                    presensiRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                    presensiRequest.setBulan(String.valueOf(position+1));
                    reqPresensi(presensiRequest);
                }
                else if(position==8)
                {
                    sessionManager = new SessionManager(getContext());
                    namabulan = "September";
                    PresensiRequest presensiRequest = new PresensiRequest();
                    presensiRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                    presensiRequest.setBulan(String.valueOf(position+1));
                    reqPresensi(presensiRequest);
                }
                else if(position==9)
                {
                    sessionManager = new SessionManager(getContext());
                    namabulan = "Oktober";
                    PresensiRequest presensiRequest = new PresensiRequest();
                    presensiRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                    presensiRequest.setBulan(String.valueOf(position+1));
                    reqPresensi(presensiRequest);
                }
                else if(position==10)
                {
                    sessionManager = new SessionManager(getContext());
                    namabulan = "November";
                    PresensiRequest presensiRequest = new PresensiRequest();
                    presensiRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                    presensiRequest.setBulan(String.valueOf(position+1));
                    Log.d("asdfgh",String.valueOf(position));
                    reqPresensi(presensiRequest);
                }
                else if(position==11)
                {
                    sessionManager = new SessionManager(getContext());
                    namabulan = "Desember";
                    PresensiRequest presensiRequest = new PresensiRequest();
                    presensiRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                    presensiRequest.setBulan(String.valueOf(position+1));
                    reqPresensi(presensiRequest);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bulan = view.findViewById(R.id.spinerbulan);
    }

    private void reqPresensi(PresensiRequest presensiRequest)
    {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("loading");
        progressDialog.show();
        progressDialog.setCancelable(false);
        EndPoint service = RetrofitInstance.getRetrofitInstance(getContext()).create(EndPoint.class);
        Call<PresensiResponse> call = service.doPresensi(presensiRequest);
        call.enqueue(new Callback<PresensiResponse>() {
            @Override
            public void onResponse(Call<PresensiResponse> call, Response<PresensiResponse> response) {
                progressDialog.hide();
                getData(response.body().getData());
            }

            @Override
            public void onFailure(Call<PresensiResponse> call, Throwable t) {

            }
        });
    }

    private void getData(List<PresensiModel>data)
    {
        presensiModelList.clear();
        presensiModelList.addAll(data);
        Log.d("oit",data.toString());
        presensiAdapter.notifyDataSetChanged();
    }
}
