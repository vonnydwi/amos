package com.example.vonny.amos_140707805;

import com.example.vonny.amos_140707805.Jadwal.JadwalRequest;
import com.example.vonny.amos_140707805.Jadwal.JadwalResponse;
import com.example.vonny.amos_140707805.Nilai.NilaiRequest;
import com.example.vonny.amos_140707805.Nilai.NilaiResponse;
import com.example.vonny.amos_140707805.Pengaduan.PengaduanRequest;
import com.example.vonny.amos_140707805.Pengaduan.PengaduanResponse;
import com.example.vonny.amos_140707805.Pengumuman.PengumumanResponse;
import com.example.vonny.amos_140707805.Presensi.PresensiRequest;
import com.example.vonny.amos_140707805.Presensi.PresensiResponse;
import com.example.vonny.amos_140707805.Sandi.SandiRequest;
import com.example.vonny.amos_140707805.Sandi.SandiResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface EndPoint {
    @GET("/pengumuman_rest")
    Call<PengumumanResponse> getPengumuman();

    @POST ("/login_rest")
    Call<LoginResponse> doLogin(@Body LoginModel data);

    @POST ("/jadwal_rest")
    Call<JadwalResponse> doJadwal(@Body JadwalRequest data);

    @POST("/nilai_rest")
    Call<NilaiResponse> doNilai(@Body NilaiRequest data);

    @POST("absensi_rest")
    Call<PresensiResponse> doPresensi(@Body PresensiRequest data);

    @POST("/pengaduan_rest")
    Call<PengaduanResponse> doPengaduan(@Body PengaduanRequest data);

    @POST("/changepassword_rest")
    Call<SandiResponse> doSandi(@Body SandiRequest data);
}
