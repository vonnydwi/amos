package com.example.vonny.amos_140707805.Presensi;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PresensiRequest {

   @SerializedName("id_siswa")
    private String idsiswa;

   @SerializedName("bulan")
   private String bulan;

    public void setIdsiswa(String idsiswa) {
        this.idsiswa = idsiswa;
    }

    public String getIdsiswa() {
        return idsiswa;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getBulan() {
        return bulan;
    }
}
