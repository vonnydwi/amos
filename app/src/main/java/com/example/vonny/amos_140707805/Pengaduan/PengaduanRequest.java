package com.example.vonny.amos_140707805.Pengaduan;

import com.google.gson.annotations.SerializedName;

public class PengaduanRequest {
    @SerializedName("id_siswa")
    private String idsiswa;

    @SerializedName("subjek")
    private  String subjek;

    @SerializedName("isi")
    private String pengaduan;

    public void setIdsiswa(String idsiswa) {
        this.idsiswa = idsiswa;
    }

    public void setPengaduan(String pengaduan) {
        this.pengaduan = pengaduan;
    }

    public void setSubjek(String subjek) {
        this.subjek = subjek;
    }

    public String getIdsiswa() {
        return idsiswa;
    }

    public String getPengaduan() {
        return pengaduan;
    }

    public String getSubjek() {
        return subjek;
    }
}
