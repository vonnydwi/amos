package com.example.vonny.amos_140707805.Jadwal;

import com.google.gson.annotations.SerializedName;

public class JadwalModel {

    @SerializedName("jam")
    private String jam;

    @SerializedName("nama_mapel")
    private String mapel;

    public void setJam(String jam) {
        this.jam = jam;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getJam() {
        return jam;
    }

    public String getMapel() {
        return mapel;
    }
}
