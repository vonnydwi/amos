package com.example.vonny.amos_140707805.Pengaduan;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.vonny.amos_140707805.EndPoint;
import com.example.vonny.amos_140707805.LoginResponse;
import com.example.vonny.amos_140707805.R;
import com.example.vonny.amos_140707805.RetrofitInstance;
import com.example.vonny.amos_140707805.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class PengaduanFragment extends Fragment {

    private EditText pengaduan, subjek;
    private RelativeLayout layoutpengaduan;
    private Button btn_pengaduan;
    private String temp;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pengaduan, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        PengaduanRequest pengaduanRequest = new PengaduanRequest();
        SessionManager sessionManager = new SessionManager(getContext());
        subjek = view.findViewById(R.id.ed_pengaduanjudul);
        pengaduan = view.findViewById(R.id.ed_pengaduan);
        btn_pengaduan = view.findViewById(R.id.btn_pengaduan);

        layoutpengaduan = view.findViewById(R.id.rv_pengaduan);
        Log.d(pengaduan.getText().toString(), "vonny");

        btn_pengaduan.setOnClickListener((View V) -> {

            if (pengaduan.getText().toString().isEmpty() || subjek.getText().toString().isEmpty()) {
                Snackbar snackbar = Snackbar.make(layoutpengaduan, "Lengkapi judul atau isi pengaduan anda.", Snackbar.LENGTH_LONG);
                snackbar.show();
                InputMethodManager inputManager = (InputMethodManager)
                        getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            } else {
                pengaduanRequest.setIdsiswa(sessionManager.getKeyIdsiswa());
                pengaduanRequest.setSubjek(subjek.getText().toString());
                pengaduanRequest.setPengaduan(pengaduan.getText().toString());
                EndPoint service = RetrofitInstance.getRetrofitInstance(getContext()).create(EndPoint.class);
                Call<PengaduanResponse> call = service.doPengaduan(pengaduanRequest);
                call.enqueue(new Callback<PengaduanResponse>() {
                    @Override
                    public void onResponse(Call<PengaduanResponse> call, Response<PengaduanResponse> response) {
                        PengaduanResponse response1 = response.body();
                        if (response1.getStatus().equals("Berhasil")) {
                            Snackbar snackbar = Snackbar.make(layoutpengaduan, "Terima kasih atas pengaduan Bapak/Ibu, pihak sekolah akan segera menghubungi.", Snackbar.LENGTH_LONG);
                            snackbar.show();
                            pengaduan.getText().clear();
                            subjek.getText().clear();
                            InputMethodManager inputManager = (InputMethodManager)
                                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);
                        }
                    }

                    @Override
                    public void onFailure(Call<PengaduanResponse> call, Throwable t) {

                    }
                });

            }
        });

    }
}
