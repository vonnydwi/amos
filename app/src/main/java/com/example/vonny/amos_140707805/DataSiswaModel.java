package com.example.vonny.amos_140707805;

import com.google.gson.annotations.SerializedName;

public class DataSiswaModel {

    @SerializedName("id_siswa")
    private String idsiswa;

    @SerializedName("nama")
    private String nama;

    @SerializedName("nisn")
    private String nisn;

    @SerializedName("nis")
    private String nis;

    @SerializedName("id_kelas")
    private String id_kelas;

    @SerializedName("tempat_lahir")
    private String tempatlahir;

    @SerializedName("tgl_lahir")
    private String tgllahir;

    @SerializedName("agama")
    private String agama;

    @SerializedName("tempat_tinggal")
    private String tempattinggal;

    @SerializedName("nama_ayah")
    private String namaayah;

    @SerializedName("gender")
    private String gender;

    @SerializedName("password")
    private String password;

    @SerializedName("foto")
    private String foto;

    @SerializedName("flg_alumni")
    private String flg_alumni;

    public void setIdsiswa(String idsiswa) {
        this.idsiswa = idsiswa;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public void setNis(String nis) {
        this.nis = nis;
    }

    public void setId_kelas(String id_kelas) {
        this.id_kelas = id_kelas;
    }

    public void setTempatlahir(String tempatlahir) {
        this.tempatlahir = tempatlahir;
    }

    public void setTgllahir(String tgllahir) {
        this.tgllahir = tgllahir;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public void setTempattinggal(String tempattinggal) {
        this.tempattinggal = tempattinggal;
    }

    public void setNamaayah(String namaayah) {
        this.namaayah = namaayah;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public void setFlg_alumni(String flg_alumni) {
        this.flg_alumni = flg_alumni;
    }

    public String getIdsiswa() {
        return idsiswa;
    }

    public String getNama() {
        return nama;
    }

    public String getNisn() {
        return nisn;
    }

    public String getNis() {
        return nis;
    }

    public String getId_kelas() {
        return id_kelas;
    }

    public String getTempatlahir() {
        return tempatlahir;
    }

    public String getTgllahir() {
        return tgllahir;
    }

    public String getAgama() {
        return agama;
    }

    public String getTempattinggal() {
        return tempattinggal;
    }

    public String getNamaayah() {
        return namaayah;
    }

    public String getGender() {
        return gender;
    }

    public String getPassword() {
        return password;
    }

    public String getFoto() {
        return foto;
    }

    public String getFlg_alumni() {
        return flg_alumni;
    }
}
