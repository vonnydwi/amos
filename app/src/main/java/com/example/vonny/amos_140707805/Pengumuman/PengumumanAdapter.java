package com.example.vonny.amos_140707805.Pengumuman;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.vonny.amos_140707805.R;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

public class PengumumanAdapter extends RecyclerView.Adapter<PengumumanAdapter.ViewHolder>{

    private List<PengumumanModel> pengumumanModelList;
    Context context;

    class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView judul, tanggal, isi;
        public RelativeLayout relativeLayout;

        public ViewHolder(View ItemView)
        {
            super(ItemView);
            judul=itemView.findViewById(R.id.tv_pgmn1);
            tanggal=itemView.findViewById(R.id.tv_pgmn2);
            isi=itemView.findViewById(R.id.tv_pgmn3);
            relativeLayout=itemView.findViewById(R.id.view_layout);
        }
    }

    public PengumumanAdapter(Context context, List<PengumumanModel> pengumumanModelList)
    {
        this.context=context;
        this.pengumumanModelList=pengumumanModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View V = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_pengumuman,parent,false);
        ViewHolder VH = new ViewHolder(V);
        return VH;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {



            PengumumanModel pengumumanmodel = pengumumanModelList.get(position);
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
// pass your DOB String
        DateTime jodatime = dtf.parseDateTime(pengumumanmodel.getTanggal());
// Format for output
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd-MMMM-yyyy");
// Print the date
        System.out.println(dtfOut.print(jodatime));
            holder.judul.setText(pengumumanmodel.getJudul());
            holder.tanggal.setText(dtfOut.print(jodatime));
            holder.isi.setText(pengumumanmodel.getIsi());



            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Bundle bundle = new Bundle();
                    bundle.putString("id_pengumuman", Integer.toString(pengumumanmodel.getId()));
                    bundle.putString("judul_pengumuman", pengumumanmodel.getJudul());
                    bundle.putString("detail_pengumuman", pengumumanmodel.getIsi());
                    bundle.putString("created_date", dtfOut.print(jodatime));
                    bundle.putString("created_by", pengumumanmodel.getPembuat());
                    bundle.putString("gambar_pengumuman",pengumumanmodel.getGambar());

                    Intent intent = new Intent(context,PengumumanDetail.class);

                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            });

    }

    @Override
    public int getItemCount() {
        return pengumumanModelList.size();
    }
}
