package com.example.vonny.amos_140707805.Pengumuman;

import com.example.vonny.amos_140707805.Pengumuman.PengumumanModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PengumumanResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private List<PengumumanModel> data;

    public void setStatus(String status) {
        this.status = status;
    }

    public void setData(List<PengumumanModel> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public List<PengumumanModel> getData() {
        return data;
    }
}
