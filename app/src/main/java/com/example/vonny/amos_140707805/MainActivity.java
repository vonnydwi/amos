package com.example.vonny.amos_140707805;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;
import com.example.vonny.amos_140707805.Jadwal.JadwalFragment;
import com.example.vonny.amos_140707805.Nilai.NilaiFragment;
import com.example.vonny.amos_140707805.Pengaduan.PengaduanFragment;
import com.example.vonny.amos_140707805.Pengumuman.PengumumanFragment;
import com.example.vonny.amos_140707805.Presensi.PresensiFragment;
import com.example.vonny.amos_140707805.Sandi.SandiFragment;



public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    TextView title, namasiswa;
    SessionManager sessionManager;
    private static final String TAG = MainActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        sessionManager = new SessionManager(this);
        title.setText("Beranda");
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.bg_splash));
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        namasiswa = hView.findViewById(R.id.zz);
        namasiswa.setText(sessionManager.getKeyNamasiswa());
        navigationView.setNavigationItemSelectedListener(this);
        displayberanda();
    }
    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if(doubleBackToExitPressedOnce) {
            super.onBackPressed(); // this
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displayScreenSelected(item.getItemId());
        return true;
    }


    private void displayberanda()
    {
        Fragment fragment = null;
        fragment = new PengumumanFragment();
        title.setText("Pengumuman");

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

    }


    private void displayScreenSelected(int itemId) {
        Fragment fragment = null;
        switch (itemId) {
            case R.id.nav_beranda:
                fragment = new PengumumanFragment();
                title.setText("Pengumuman");
                break;
            case R.id.nav_nilai:
                fragment = new NilaiFragment();
                title.setText("Nilai");
                break;
            case R.id.nav_presensi:
                fragment = new PresensiFragment();
                title.setText("Presensi");
                break;
            case R.id.nav_jadwal:
                fragment = new JadwalFragment();
                title.setText("Jadwal");
                break;
            case R.id.nav_PROFIL:
                fragment = new ProfilFragment();
                title.setText("Profil");
                break;
            case R.id.nav_sandi:
                fragment = new SandiFragment();
                title.setText("Ubah Kata Sandi");
                break;
            case R.id.nav_pengaduan:
                fragment = new PengaduanFragment();
                title.setText("Pengaduan");
                break;

            case R.id.nav_logout:
                FragmentManager fm = getSupportFragmentManager();
                title.setText("Keluar");
                KeluarFragment keluarFragment = new KeluarFragment();
                keluarFragment.show(fm,"a");
                break;
        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }
}
