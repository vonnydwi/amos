package com.example.vonny.amos_140707805.Nilai;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.example.vonny.amos_140707805.EndPoint;
import com.example.vonny.amos_140707805.Jadwal.JadwalAdapter;
import com.example.vonny.amos_140707805.Jadwal.JadwalModel;
import com.example.vonny.amos_140707805.Jadwal.JadwalRequest;
import com.example.vonny.amos_140707805.Jadwal.JadwalResponse;
import com.example.vonny.amos_140707805.R;
import com.example.vonny.amos_140707805.RetrofitInstance;
import com.example.vonny.amos_140707805.SessionManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NilaiFragment extends Fragment {

    String mapel = "Matematika";
    Spinner spinnername;
    SessionManager sessionManager;

    public List<NilaiModel> nilaiModelList = new ArrayList<>();
    private NilaiAdapter nilaiAdapter;
    RecyclerView recyclerView;
    ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nilai, container, false);
    }

    private void getdataNilai(NilaiRequest nilaiRequest)
    {

        EndPoint service = RetrofitInstance.getRetrofitInstance(getContext()).create(EndPoint.class);
        Call<NilaiResponse> call = service.doNilai(nilaiRequest);
        call.enqueue(new Callback<NilaiResponse>() {
            @Override
            public void onResponse(Call<NilaiResponse> call, Response<NilaiResponse> response) {
                getData(response.body().getData());
                Log.d("oit",response.body().getData().toString());

            }

            @Override
            public void onFailure(Call<NilaiResponse> call, Throwable t) {

            }
        });
    }

    private void getData(List<NilaiModel> data) {
        progressDialog.hide();
        nilaiModelList.clear();
        nilaiModelList.addAll(data);
        nilaiAdapter.notifyDataSetChanged();
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.rv_nilai);
        nilaiAdapter = new NilaiAdapter(getContext(),nilaiModelList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(nilaiAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),LinearLayoutManager.HORIZONTAL));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),LinearLayoutManager.VERTICAL));

        spinnername = view.findViewById(R.id.spinernilai);
       // spinnername.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){}

        spinnername.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("loading");
                progressDialog.show();
                progressDialog.setCancelable(false);
                if(position==0)
                {
                    sessionManager = new SessionManager(getContext());
                    mapel="Matematika";
                    NilaiRequest request = new NilaiRequest();
                    request.setIdsiswa(sessionManager.getKeyIdsiswa());
                    request.setIdmapel(mapel);
                    request.setIdkelas(sessionManager.getKeyIdKelassiswa());
                    getdataNilai(request);

                }
                else if (position==1)
                {
                    sessionManager = new SessionManager(getContext());
                    mapel="Bahasa Indonesia";
                    NilaiRequest request = new NilaiRequest();
                    request.setIdsiswa(sessionManager.getKeyIdsiswa());
                    request.setIdmapel(mapel);
                    request.setIdkelas(sessionManager.getKeyIdKelassiswa());
                    getdataNilai(request);
                }
                else if (position==2)
                {
                    sessionManager = new SessionManager(getContext());
                    mapel="Bahasa Inggris";
                    NilaiRequest request = new NilaiRequest();
                    request.setIdsiswa(sessionManager.getKeyIdsiswa());
                    request.setIdmapel(mapel);
                    request.setIdkelas(sessionManager.getKeyIdKelassiswa());
                    getdataNilai(request);
                }
                else if (position==3)
                {
                    sessionManager = new SessionManager(getContext());
                    mapel="Seni Budaya";
                    NilaiRequest request = new NilaiRequest();
                    request.setIdsiswa(sessionManager.getKeyIdsiswa());
                    request.setIdmapel(mapel);
                    request.setIdkelas(sessionManager.getKeyIdKelassiswa());
                    getdataNilai(request);
                }
                else if (position==4)
                {
                    sessionManager = new SessionManager(getContext());
                    mapel="Agama";
                    NilaiRequest request = new NilaiRequest();
                    request.setIdsiswa(sessionManager.getKeyIdsiswa());
                    request.setIdmapel(mapel);
                    request.setIdkelas(sessionManager.getKeyIdKelassiswa());
                    getdataNilai(request);
                }
                else if (position==5)
                {
                    sessionManager = new SessionManager(getContext());
                    mapel="Akutansi";
                    NilaiRequest request = new NilaiRequest();
                    request.setIdsiswa(sessionManager.getKeyIdsiswa());
                    request.setIdmapel(mapel);
                    request.setIdkelas(sessionManager.getKeyIdKelassiswa());
                    getdataNilai(request);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


}
