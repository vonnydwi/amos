package com.example.vonny.amos_140707805.Nilai;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.vonny.amos_140707805.Jadwal.JadwalAdapter;
import com.example.vonny.amos_140707805.R;

import java.util.List;

public class NilaiAdapter extends RecyclerView.Adapter<NilaiAdapter.ViewHolder> {

    private List<NilaiModel> nilaiModelList;
    Context context;

    class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView kd1,kd2,kd3,kd4,kd5;
        public ViewHolder(View ItemView)
        {
            super(ItemView);
            kd1=itemView.findViewById(R.id.kd1);
            kd2=itemView.findViewById(R.id.kd2);
            kd3=itemView.findViewById(R.id.kd3);
            kd4=itemView.findViewById(R.id.kd4);
            kd5=itemView.findViewById(R.id.kd5);

        }
    }

    public NilaiAdapter(Context context,List<NilaiModel>nilaiModelList)
    {
        this.context=context;
        this.nilaiModelList=nilaiModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View V = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_detailnilai,parent,false);
        NilaiAdapter.ViewHolder VH = new NilaiAdapter.ViewHolder(V);
        return VH;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(position == 0) {
            holder.kd1.setText("KD");
            holder.kd2.setText("Tugas");
            holder.kd3.setText("PR");
            holder.kd4.setText("Ulangan");
            holder.kd5.setText("Ujian");
        } else {
            NilaiModel nilaiModel = nilaiModelList.get(position - 1);
            holder.kd1.setText(nilaiModel.getNokd());
            holder.kd2.setText(nilaiModel.getTugasp());
            holder.kd3.setText(nilaiModel.getTugashp());
            holder.kd4.setText(nilaiModel.getTugasup());
            holder.kd5.setText(nilaiModel.getTugasap());
            if(nilaiModel.getTugasap()==null) {
                holder.kd5.setText("-");
            }

            if(nilaiModel.getTugashp()==null) {
                holder.kd3.setText("-");
            }
            if(nilaiModel.getTugasup()==null) {
                holder.kd4.setText("-");
            }
            if(nilaiModel.getTugasp()==null) {
                holder.kd2.setText("-");
            }
        }
    }

    @Override
    public int getItemCount() {
        return nilaiModelList.size() + 1;
    }
}
