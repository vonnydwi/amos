package com.example.vonny.amos_140707805.Sandi;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.vonny.amos_140707805.EndPoint;
import com.example.vonny.amos_140707805.Pengaduan.PengaduanResponse;
import com.example.vonny.amos_140707805.R;
import com.example.vonny.amos_140707805.RetrofitInstance;
import com.example.vonny.amos_140707805.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class SandiFragment extends Fragment {

    private RelativeLayout layoutsandi;
    private EditText username, sandilama, sandibaru;
    private Button button;
    SessionManager sessionManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sandi, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SandiRequest sandiRequest = new SandiRequest();
        SessionManager sessionManager = new SessionManager(getContext());

        button = view.findViewById(R.id.btnsandi);
        username = view.findViewById(R.id.sandi_username);
        sandilama = view.findViewById(R.id.sandi_sandilama);
        sandibaru = view.findViewById(R.id.sandi_sandibaru);
        layoutsandi = view.findViewById(R.id.rv_sandi);

        sessionManager = new SessionManager(getContext());
        username.setText(sessionManager.getKeyNisnsiswa());

        button.setOnClickListener((View V)->{

            if (username.getText().toString().isEmpty() || sandilama.getText().toString().isEmpty() || sandibaru.getText().toString().isEmpty())
            {
                Snackbar snackbar = Snackbar.make(layoutsandi,"Tidak boleh ada yang kosong",Snackbar.LENGTH_LONG);
                snackbar.show();
                InputMethodManager inputManager = (InputMethodManager)
                        getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);


            }
            else {
                sandiRequest.setUsername(username.getText().toString());
                sandiRequest.setPassword(sandilama.getText().toString());
                sandiRequest.setNewpass(sandibaru.getText().toString());

                EndPoint service = RetrofitInstance.getRetrofitInstance(getContext()).create(EndPoint.class);
                Call<SandiResponse> call = service.doSandi(sandiRequest);
                call.enqueue(new Callback<SandiResponse>() {
                    @Override
                    public void onResponse(Call<SandiResponse> call, Response<SandiResponse> response) {
                        SandiResponse response1 = response.body();
                        if(response1.getStatus().equals("true"))
                        {
                            Snackbar snackbar = Snackbar.make(layoutsandi,"Pasword berhasil di ganti",Snackbar.LENGTH_LONG);
                            snackbar.show();
                            sandilama.getText().clear();
                         //   username.getText().clear();
                            sandibaru.getText().clear();
                            InputMethodManager inputManager = (InputMethodManager)
                                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);
                        }
                        else {

                            Snackbar snackbar = Snackbar.make(layoutsandi,"Cek lagi password lama yang anda masukkan",Snackbar.LENGTH_LONG);
                            snackbar.show();
                            sandilama.getText().clear();
                        //    username.getText().clear();
                            sandibaru.getText().clear();
                            InputMethodManager inputManager = (InputMethodManager)
                                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);
                        }
                    }

                    @Override
                    public void onFailure(Call<SandiResponse> call, Throwable t) {

                    }
                });
            }});

    }
}
