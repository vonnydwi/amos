package com.example.vonny.amos_140707805;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class KeluarFragment extends DialogFragment {
    private Button btn_logout, btn_cancel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_keluar, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_logout = view.findViewById(R.id.btn_logout);
        btn_cancel = view.findViewById(R.id.btn_cancel);

        btn_cancel.setOnClickListener(view1 -> {
                    Intent intent = new Intent(getContext(),MainActivity.class);
                    intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    ActivityCompat.finishAffinity(getActivity());
                }

        );

        btn_logout.setOnClickListener(view1 -> {

            new SessionManager(getContext()).logoutUser();
            Intent intent = new Intent(getContext(),LoginActivity.class);
            startActivity(intent);
            ActivityCompat.finishAffinity(getActivity());

        });
    }
}
