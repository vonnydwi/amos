package com.example.vonny.amos_140707805.Pengumuman;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vonny.amos_140707805.R;
import com.squareup.picasso.Picasso;

public class PengumumanDetail extends AppCompatActivity {

    private TextView judul, tanggal, isi;
    private ImageView imageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_pengumuman);

        imageView = findViewById(R.id.img_pgmn);
        judul = findViewById(R.id.detailpgmuman1);
        tanggal = findViewById(R.id.detailpgmuman2);
        isi = findViewById(R.id.detailpgmuman3);

        Bundle b = getIntent().getExtras();
        Picasso.with(PengumumanDetail.this).load(b.getString("gambar_pengumuman")).priority(Picasso.Priority.HIGH).into(imageView);
        judul.setText(b.getString("judul_pengumuman"));
        tanggal.setText(b.getString("created_date"));
        isi.setText(b.getString("detail_pengumuman"));

        TextView toolbarTitle = findViewById(R.id.titlepengumuman);
        toolbarTitle.setText("PENGUMUMAN");

        ImageView backButton = findViewById(R.id.backpengumuman);
        backButton.setOnClickListener(view -> {
            finish();
        });
    }
}
