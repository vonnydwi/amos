package com.example.vonny.amos_140707805.Jadwal;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vonny.amos_140707805.R;

import java.util.List;

public class JadwalAdapter extends RecyclerView.Adapter<JadwalAdapter.ViewHolder> {

    private List<JadwalModel> jadwalModelList;
    Context context;

    class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView jam, mapel, jamValue;

        public ViewHolder(View Itemview)
        {
            super(Itemview);
            jam=itemView.findViewById(R.id.jadwal_jam);
            mapel=itemView.findViewById(R.id.jadwal_mapel);
            jamValue=itemView.findViewById(R.id.jam);
        }
    }

    public JadwalAdapter(Context context, List<JadwalModel> jadwalModelList)
    {
        this.context=context;
        this.jadwalModelList=jadwalModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View V = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_jadwal,parent,false);
        ViewHolder VH = new ViewHolder(V);
        return VH;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        JadwalModel jadwalModel = jadwalModelList.get(position);
        holder.jam.setText(jadwalModel.getJam());
        holder.mapel.setText(jadwalModel.getMapel());
        switch (position) {
            case 0: holder.jamValue.setText("07.00 - 08.00");
                    break;
            case 1:  holder.jamValue.setText("08.00 - 09.00");
                    break;
            case 2:  holder.jamValue.setText("09.15 - 10.15");
                break;
            case 3:  holder.jamValue.setText("10.15 - 11.15");
                break;
            case 4:  holder.jamValue.setText("11.15 - 12.15");
                break;
            case 5:  holder.jamValue.setText("12.30 - 13.30");
                break;
            case 6:  holder.jamValue.setText("13.30 - 14.30");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return jadwalModelList.size();
    }
}
