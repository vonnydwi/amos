package com.example.vonny.amos_140707805.Sandi;

import com.google.gson.annotations.SerializedName;

public class SandiRequest {

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    @SerializedName("newpassword")
    private String newpass;

    public void setNewpass(String newpass) {
        this.newpass = newpass;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNewpass() {
        return newpass;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
}
