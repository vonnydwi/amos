package com.example.vonny.amos_140707805;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private SessionManager sessionManager;
    private EditText email, password;
    private Button btnlogin;
    ProgressDialog progressDialog;
    private RelativeLayout layoutlogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);

            email = findViewById(R.id.ed_login1);
            password = findViewById(R.id.ed_login2);
            btnlogin = findViewById(R.id.btn_login);
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("loading");
            layoutlogin = findViewById(R.id.rv_login);
            btnlogin.setOnClickListener(
                    (View v) -> {

                if(email.getText().toString().isEmpty() || password.getText().toString().isEmpty())
                {

                    Snackbar snackbar = Snackbar.make(layoutlogin,"Username atau Pasword tidak boleh kosong!",Snackbar.LENGTH_LONG);
                    snackbar.show();
                    InputMethodManager inputManager = (InputMethodManager)
                            this.getSystemService(Context.INPUT_METHOD_SERVICE);

                    inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
                else {
                    progressDialog.show();
                LoginModel loginModel = new LoginModel();
                loginModel.setNisn(email.getText().toString());
                loginModel.setPass_nisn(password.getText().toString());
                EndPoint service = RetrofitInstance.getRetrofitInstance(LoginActivity.this).create(EndPoint.class);
                Call<LoginResponse> call = service.doLogin(loginModel);
                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) { progressDialog.dismiss();
                        LoginResponse response1 = response.body();
                        if (response1.getStatus().equals("success")) {
                            sessionManager = new SessionManager(LoginActivity.this);
                            sessionManager.createLoginSession(email.getText().toString(),
                                    response1.getData().get(0).getIdsiswa(),
                                    response1.getData().get(0).getNama(),
                                    response1.getData().get(0).getNisn(),
                                    response1.getData().get(0).getNis(),
                                    response1.getData().get(0).getId_kelas(),
                                    response1.getData().get(0).getTempatlahir(),
                                    response1.getData().get(0).getTgllahir(),
                                    response1.getData().get(0).getAgama(),
                                    response1.getData().get(0).getTempattinggal(),
                                    response1.getData().get(0).getNamaayah(),
                                    response1.getData().get(0).getGender(),
                                    response1.getData().get(0).getPassword(),
                                    response1.getData().get(0).getFoto(),
                                    response1.getData().get(0).getFlg_alumni());
                            Log.d("oyyyyy", "test :" + sessionManager.getKeyKelaminsiswa());
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, "email atau password salah", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        Toast.makeText(LoginActivity.this, "Tidak ada koneksi internet", Toast.LENGTH_SHORT).show();
                    }
                });
            }});

        }
    }
