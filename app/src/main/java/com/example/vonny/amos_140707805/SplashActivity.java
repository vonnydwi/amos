package com.example.vonny.amos_140707805;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;



public class SplashActivity extends AppCompatActivity {

    private SessionManager sessionmanager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        sessionmanager = new SessionManager(getApplicationContext());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                Intent i = new Intent(getBaseContext(),LoginActivity.class);
//                startActivity(i);
//                finish();


                if (sessionmanager.isLoggedIn())
                {

                    startActivity(new Intent(getBaseContext(), MainActivity.class));
                    finish();
                }
                else
                {

                    startActivity(new Intent(getBaseContext(), LoginActivity.class));
                    finish();
                }

            }
        }, 2000);


    }





}
