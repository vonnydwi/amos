package com.example.vonny.amos_140707805.Nilai;

import com.google.gson.annotations.SerializedName;

public class NilaiModel {

    @SerializedName("no_kd")
    private String nokd;

    @SerializedName("tugasP")
    private String tugasp;

    @SerializedName("tugasHP")
    private String tugashp;

    @SerializedName("tugasUP")
    private String tugasup;

    @SerializedName("tugasAP")
    private String tugasap;

    public void setNokd(String nokd) {
        this.nokd = nokd;
    }

    public void setTugasap(String tugasap) {
        this.tugasap = tugasap;
    }

    public void setTugashp(String tugashp) {
        this.tugashp = tugashp;
    }

    public void setTugasp(String tugasp) {
        this.tugasp = tugasp;
    }

    public void setTugasup(String tugasup) {
        this.tugasup = tugasup;
    }

    public String getNokd() {
        return nokd;
    }

    public String getTugasap() {
        return tugasap;
    }

    public String getTugashp() {
        return tugashp;
    }

    public String getTugasp() {
        return tugasp;
    }

    public String getTugasup() {
        return tugasup;
    }
}
