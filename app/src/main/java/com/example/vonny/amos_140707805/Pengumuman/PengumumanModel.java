package com.example.vonny.amos_140707805.Pengumuman;

import com.google.gson.annotations.SerializedName;

public class PengumumanModel {
    @SerializedName("id_pengumuman")
    private int id;

    @SerializedName("judul_pengumuman")
    private String judul;

    @SerializedName("detail_pengumuman")
    private String isi;

    @SerializedName("created_date")
    private String tanggal;

    @SerializedName("created_by")
    private String pembuat;

    @SerializedName("gambar_pengumuman")
    private String gambar;

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPembuat(String pembuat) {
        this.pembuat = pembuat;
    }

    public String getTanggal() {
        return tanggal;
    }

    public String getJudul() {
        return judul;
    }

    public String getIsi() {
        return isi;
    }

    public int getId() {
        return id;
    }

    public String getPembuat() {
        return pembuat;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getGambar() {
        return gambar;
    }
}
