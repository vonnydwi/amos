package com.example.vonny.amos_140707805.Jadwal;

import com.google.gson.annotations.SerializedName;

public class JadwalRequest {

    @SerializedName("kelas")
    private String kelas;

    @SerializedName("hari")
    private String hari;

    public void setHari(String hari) {
        this.hari = hari;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getHari() {
        return hari;
    }

    public String getKelas() {
        return kelas;
    }
}
