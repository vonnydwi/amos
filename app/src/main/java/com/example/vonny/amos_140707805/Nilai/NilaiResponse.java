package com.example.vonny.amos_140707805.Nilai;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NilaiResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private List<NilaiModel> data;

    public void setData(List<NilaiModel> data) {
        this.data = data;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public List<NilaiModel> getData() {
        return data;
    }
}
