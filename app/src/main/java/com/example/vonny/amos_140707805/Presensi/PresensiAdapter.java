package com.example.vonny.amos_140707805.Presensi;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vonny.amos_140707805.R;

import java.util.ArrayList;
import java.util.List;

public class PresensiAdapter extends RecyclerView.Adapter<PresensiAdapter.ViewHolder> {

    private List<PresensiModel> presensiModelList;
    Context context;

    private List<String> tampungpresensi = new ArrayList<>();
    private List<String> tampunghadir = new ArrayList<>();
    private List<String> tampungalfa = new ArrayList<>();
    private List<String> tampungijin = new ArrayList<>();


    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView ketpres, tgl1, tgl2, tgl3, tgl4, tgl5, tgl6, tgl7, tgl8, tgl9, tgl10, tgl11, tgl12, tgl13, tgl14, tgl15, tgl16, tgl17, tgl18, tgl19, tgl20, tgl21, tgl22, tgl23, tgl24, tgl25, tgl26, tgl27, tgl28, tgl29, tgl30, tgl31;

        public ViewHolder(View ItemView) {
            super(ItemView);

            tgl1 = itemView.findViewById(R.id.tg11);
            tgl2 = itemView.findViewById(R.id.tgl2);
            tgl3 = itemView.findViewById(R.id.tgl3);
            tgl4 = itemView.findViewById(R.id.tgl4);
            tgl5 = itemView.findViewById(R.id.tgl5);
            tgl6 = itemView.findViewById(R.id.tgl6);
            tgl7 = itemView.findViewById(R.id.tgl7);
            tgl8 = itemView.findViewById(R.id.tgl8);
            tgl9 = itemView.findViewById(R.id.tgl9);
            tgl10 = itemView.findViewById(R.id.tgl10);
            tgl11 = itemView.findViewById(R.id.tgl11);
            tgl12 = itemView.findViewById(R.id.tgl12);
            tgl13 = itemView.findViewById(R.id.tgl13);
            tgl14 = itemView.findViewById(R.id.tgl14);
            tgl15 = itemView.findViewById(R.id.tgl15);
            tgl16 = itemView.findViewById(R.id.tgl16);
            tgl17 = itemView.findViewById(R.id.tgl17);
            tgl18 = itemView.findViewById(R.id.tgl18);
            tgl19 = itemView.findViewById(R.id.tgl19);
            tgl20 = itemView.findViewById(R.id.tgl20);
            tgl21 = itemView.findViewById(R.id.tgl21);
            tgl22 = itemView.findViewById(R.id.tgl22);
            tgl23 = itemView.findViewById(R.id.tgl23);
            tgl24 = itemView.findViewById(R.id.tgl24);
            tgl25 = itemView.findViewById(R.id.tgl25);
            tgl26 = itemView.findViewById(R.id.tgl26);
            tgl27 = itemView.findViewById(R.id.tgl27);
            tgl28 = itemView.findViewById(R.id.tgl28);
            tgl29 = itemView.findViewById(R.id.tgl29);
            tgl30 = itemView.findViewById(R.id.tgl30);
            tgl31 = itemView.findViewById(R.id.tgl31);
            ketpres = itemView.findViewById(R.id.ketpres);
        }
    }

    public PresensiAdapter(Context context, List<PresensiModel> presensiModelList) {
        this.context = context;
        this.presensiModelList = presensiModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View V = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_presensi, parent, false);
        PresensiAdapter.ViewHolder VH = new PresensiAdapter.ViewHolder(V);
        return VH;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PresensiModel presensiModel = presensiModelList.get(position);

        holder.tgl1.setText(" 1.  "+presensiModel.getH1());
        holder.tgl2.setText("2.  "+presensiModel.getH2());
        holder.tgl3.setText("3.  "+presensiModel.getH3());
        holder.tgl4.setText("4.  "+presensiModel.getH4());
        holder.tgl5.setText("5.  "+presensiModel.getH5());
        holder.tgl6.setText("6.  "+presensiModel.getH6());
        holder.tgl7.setText("7.  "+presensiModel.getH7());
        holder.tgl8.setText("8.  "+presensiModel.getH8());
        holder.tgl9.setText("9.  "+presensiModel.getH9());
        holder.tgl10.setText("10.  "+presensiModel.getH10());
        holder.tgl11.setText("11.  "+presensiModel.getH11());
        holder.tgl12.setText("12.  "+presensiModel.getH12());
        holder.tgl13.setText("13.  "+presensiModel.getH13());
        holder.tgl14.setText("14.  "+presensiModel.getH14());
        holder.tgl15.setText("15.  "+presensiModel.getH15());
        holder.tgl16.setText("16.  "+presensiModel.getH16());
        holder.tgl17.setText("17.  "+presensiModel.getH17());
        holder.tgl18.setText("18.  "+presensiModel.getH18());
        holder.tgl19.setText("19.  "+presensiModel.getH19());
        holder.tgl20.setText("20.  "+presensiModel.getH20());
        holder.tgl21.setText("21.  "+presensiModel.getH21());
        holder.tgl22.setText("22.  "+presensiModel.getH22());
        holder.tgl23.setText("23.  "+presensiModel.getH23());
        holder.tgl24.setText("24.  "+presensiModel.getH24());
        holder.tgl25.setText("25.  "+presensiModel.getH25());
        holder.tgl26.setText("26.  "+presensiModel.getH26());
        holder.tgl27.setText("27.  "+presensiModel.getH27());
        holder.tgl28.setText("28.  "+presensiModel.getH28());
        holder.tgl29.setText("29.  "+presensiModel.getH29());
        holder.tgl30.setText("30.  "+presensiModel.getH30());
        holder.tgl31.setText("31.  "+presensiModel.getH31());

        getPresensi(presensiModel);
        conditionalHolder(holder, presensiModel);
        countPresensi(holder);
    }

    private void conditionalHolder(ViewHolder holder, PresensiModel presensiModel) {
        if(presensiModel.getH1()==null)
        {
            holder.tgl1.setText(" 1.  -");
        }
        if(presensiModel.getH2()==null)
        {
            holder.tgl2.setText("2.  -");
        }
        if(presensiModel.getH3()==null)
        {
            holder.tgl3.setText("3.  -");
        }
        if(presensiModel.getH4()==null)
        {
            holder.tgl4.setText("4.  -");
        }
        if(presensiModel.getH5()==null)
        {
            holder.tgl5.setText("5.  -");
        }
        if(presensiModel.getH6()==null)
        {
            holder.tgl6.setText("6.  -");
        }
        if(presensiModel.getH7()==null)
        {
            holder.tgl7.setText("7.  -");
        }
        if(presensiModel.getH8()==null)
        {
            holder.tgl8.setText("8.  -");
        }
        if(presensiModel.getH9()==null)
        {
            holder.tgl9.setText("9.  -");
        }
        if(presensiModel.getH10()==null)
        {
            holder.tgl10.setText("10.  -");
        }
        if(presensiModel.getH11()==null)
        {
            holder.tgl11.setText("11.  -");
        }
        if(presensiModel.getH12()==null)
        {
            holder.tgl12.setText("12.  -");
        }
        if(presensiModel.getH13()==null)
        {
            holder.tgl13.setText("13.  -");
        }
        if(presensiModel.getH14()==null)
        {
            holder.tgl14.setText("14.  -");
        }
        if(presensiModel.getH15()==null)
        {
            holder.tgl15.setText("15.  -");
        }
        if(presensiModel.getH16()==null)
        {
            holder.tgl16.setText("16.  -");
        }
        if(presensiModel.getH17()==null)
        {
            holder.tgl17.setText("17.  -");
        }
        if(presensiModel.getH18()==null)
        {
            holder.tgl18.setText("18.  -");
        }
        if(presensiModel.getH19()==null)
        {
            holder.tgl19.setText("19.  -");
        }
        if(presensiModel.getH20()==null)
        {
            holder.tgl20.setText("20.  -");
        }
        if(presensiModel.getH21()==null)
        {
            holder.tgl21.setText("21.  -");
        }
        if(presensiModel.getH22()==null)
        {
            holder.tgl22.setText("22.  -");
        }
        if(presensiModel.getH23()==null)
        {
            holder.tgl23.setText("23.  -");
        }
        if(presensiModel.getH24()==null)
        {
            holder.tgl24.setText("24.  -");
        }
        if(presensiModel.getH25()==null)
        {
            holder.tgl25.setText("25.  -");
        }
        if(presensiModel.getH26()==null)
        {
            holder.tgl26.setText("26.  -");
        }
        if(presensiModel.getH27()==null)
        {
            holder.tgl27.setText("27.  -");
        }
        if(presensiModel.getH28()==null)
        {
            holder.tgl28.setText("28.  -");
        }
        if(presensiModel.getH29()==null)
        {
            holder.tgl29.setText("29.  -");
        }
        if(presensiModel.getH30()==null)
        {
            holder.tgl30.setText("30.  -");
        }
        if(presensiModel.getH31()==null)
        {
            holder.tgl31.setText("31.  -");
        }
    }

    private void getPresensi(PresensiModel presensiModel) {
        tampungpresensi.clear();
        tampunghadir.clear();
        tampungalfa.clear();
        tampungijin.clear();
        tampungpresensi.add(presensiModel.getH1());
        tampungpresensi.add(presensiModel.getH2());
        tampungpresensi.add(presensiModel.getH3());
        tampungpresensi.add(presensiModel.getH4());
        tampungpresensi.add(presensiModel.getH5());
        tampungpresensi.add(presensiModel.getH6());
        tampungpresensi.add(presensiModel.getH7());
        tampungpresensi.add(presensiModel.getH8());
        tampungpresensi.add(presensiModel.getH9());
        tampungpresensi.add(presensiModel.getH10());
        tampungpresensi.add(presensiModel.getH11());
        tampungpresensi.add(presensiModel.getH12());
        tampungpresensi.add(presensiModel.getH13());
        tampungpresensi.add(presensiModel.getH14());
        tampungpresensi.add(presensiModel.getH15());
        tampungpresensi.add(presensiModel.getH16());
        tampungpresensi.add(presensiModel.getH17());
        tampungpresensi.add(presensiModel.getH18());
        tampungpresensi.add(presensiModel.getH19());
        tampungpresensi.add(presensiModel.getH20());
        tampungpresensi.add(presensiModel.getH21());
        tampungpresensi.add(presensiModel.getH22());
        tampungpresensi.add(presensiModel.getH23());
        tampungpresensi.add(presensiModel.getH24());
        tampungpresensi.add(presensiModel.getH25());
        tampungpresensi.add(presensiModel.getH26());
        tampungpresensi.add(presensiModel.getH27());
        tampungpresensi.add(presensiModel.getH28());
        tampungpresensi.add(presensiModel.getH29());
        tampungpresensi.add(presensiModel.getH30());
        tampungpresensi.add(presensiModel.getH31());
    }

    private void countPresensi(ViewHolder holder) {

      //  Log.d("VONNY", "Ukuran list" + String.valueOf(tampungpresensi.size()));

        for(int i=0;i<tampungpresensi.size();i++) {
            if(tampungpresensi.get(i)!= null)
            {
                if(tampungpresensi.get(i).equalsIgnoreCase("H")) {
                    tampunghadir.add(tampungpresensi.get(i));
                } else if(tampungpresensi.get(i).equalsIgnoreCase("I")) {
                    tampungijin.add(tampungpresensi.get(i));
                } else {
                    tampungalfa.add(tampungpresensi.get(i));
                }
            }

        }
    holder.ketpres.setText("Hadir = "+String.valueOf(tampunghadir.size())+",     Ijin = "+String.valueOf(tampungijin.size())+",     Alfa = "+String.valueOf(tampungalfa.size()));

    }

    @Override
    public int getItemCount() {
        return presensiModelList.size();
    }
}
